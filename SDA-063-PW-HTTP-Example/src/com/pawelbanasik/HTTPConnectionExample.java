package com.pawelbanasik;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;

public class HTTPConnectionExample {

	String ua = "Pawel/1.0";

	public String sendGET(String url) throws IOException {

		// definujemy adres polaczenia (hosta)
		// String url = "http://wp.pl";

		// definujemy przegladrke (user-agent)

		// definiujemy obiekt typu url - odwolujemy sie pod wskazany adres;
		URL obj = new URL(url);

		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", ua);

		String ret = "", currentLine = "";

		if (con.getResponseCode() == 200) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			while ((currentLine = br.readLine()) != null) {
				ret += currentLine;
			}
			br.close();
		}

		return ret;

	}

	public String sendPOST(String url) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", ua);

		String myParams = "klucz=wartosc&innyklucz=innawartosc";
		myParams = "login=test&haslo=qwerty";

		con.setDoOutput(true);
		DataOutputStream dos = new DataOutputStream(con.getOutputStream());
		dos.writeBytes(myParams);
		dos.flush();
		dos.close();

		// koniec posta

		String ret = "";
		if (con.getResponseCode() == 200) {
			Scanner s = new Scanner(new InputStreamReader(con.getInputStream()));
			while (s.hasNextLine()) {
				ret += s.nextLine();
			}
			s.close();
		}
		return ret;
	}

	
}
